Cours : Thinking in React
---------------------------------
```
	$ bower install react

	$ bower install bootstrap

	$ npm install --save httpster

```

Update "package.json"
	
	##replace line :

		"test": "echo \"Error: no test specified\" && exit 1"

	##by

		"start": "open http://localhost:4321 & httpster -p 4321"

	##and run server with $ npm start

------------------------------------------------------------------------------------
After HTML-VERSION branch
Break the UI into a component hierarchy

We have a look at the UI and identify the following components:
	
	EpisodeRow
	
	EpisodeTable
	
	SearchBar
	
	FilterableEpisodeTable - contains the other components
